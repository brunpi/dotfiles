"git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
"vim +PluginInstall +qall
" ==================VUNDLE====================
set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

Plugin 'vim-syntastic/syntastic'

Plugin 'altercation/vim-colors-solarized'

Plugin 'SirVer/ultisnips'
Plugin 'akretion/vim-odoo-snippets'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" ==================VUNDLE====================

" enable syntax highlighting
syntax enable

" color scheme
set background=dark
let g:solarized_termcolors=256
colorscheme solarized

" show line numbers
set number relativenumber
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" set tabs to have 4 spaces
set ts=4

" indent when moving to the next line while writing code
set autoindent

" expand tabs into spaces
set expandtab

" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4

" show the matching part of the pair for [] {} and ()
set showmatch

" enable all Python syntax highlighting features
let python_highlight_all = 1

" share clipboard across apps
set clipboard=unnamedplus

" highlight column 81 (and after 120)
let &colorcolumn="80,".join(range(120,999),",")

" highligth and show search
set incsearch
set hlsearch

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
imap <F8> :SyntasticCheck

let g:syntastic_python_checkers = ['flake8', 'pylint']
let g:syntastic_python_pylint_args = "--load-plugins=pylint_odoo --disable=api-one-deprecated,api-one-multi-together,attribute-deprecated,class-camelcase,create-user-wo-reset-password,consider-merging-classes-inherited,copy-wo-api-one,dangerous-filter-wo-user,dangerous-view-replace-wo-priority,deprecated-module,duplicate-id-csv,duplicate-xml-fields,duplicate-xml-record-id,file-not-used,incoherent-interpreter-exec-perm,invalid-commit,manifest-deprecated-key,method-compute,method-inverse,method-required-super,method-search,missing-newline-extrafiles,missing-readme,no-utf8-coding-comment,unnecessary-utf8-coding-comment,odoo-addons-relative-import,old-api7-method-defined,openerp-exception-warning,redundant-modulename-xml,sql-injection,too-complex,translation-field,translation-required,use-vim-comment,wrong-tabs-instead-of-spaces,xml-syntax-error,import-error,missing-docstring,too-few-public-methods,attribute-string-redundant,protected-access"
let g:syntastic_aggregate_errors = 1
let g:syntastic_javascript_checkers = ['eslint']

" UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
