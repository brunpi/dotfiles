" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
call plug#begin('~/.local/share/nvim/plugged')

Plug 'iCyMind/NeoSolarized'

Plug 'SirVer/ultisnips' | Plug 'akretion/vim-odoo-snippets'

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" Initialize plugin system
call plug#end()

" solarized colors
set termguicolors
colorscheme NeoSolarized

" share clipboard across apps
set clipboard+=unnamedplus

" show line numbers
set number relativenumber
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Use deoplete (autocompletion)
let g:deoplete#enable_at_startup = 1
call deoplete#custom#source('ultisnips', 'matchers', ['matcher_fuzzy'])

" highlight column 81 (and after 120)
let &colorcolumn="80,".join(range(120,999),",")

" set tabs to have 4 spaces
set ts=4
" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4
